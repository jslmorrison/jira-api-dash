<?php

namespace spec\App\Service;

use App\Service\ApiClient;
use PhpSpec\ObjectBehavior;

class ApiClientSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ApiClient::class);
        $this->shouldImplement(\App\Service\HttpClient::class);
    }

    public function let()
    {
        $this->beConstructedWith('jslmorrison@gmail.com', 'some-token-string', 'https://the-api-url');
    }

    public function it_should_have_a_http_client()
    {
        $this->client()->shouldBeAnInstanceOf(\GuzzleHttp\Client::class);
    }

    public function it_should_have_a_username()
    {
        $this->username()->shouldReturn('jslmorrison@gmail.com');
    }

    public function it_should_have_a_token()
    {
        $this->token()->shouldReturn('some-token-string');
    }
}
