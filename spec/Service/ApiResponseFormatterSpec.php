<?php

namespace spec\App\Service;

use App\Service\ApiResponseFormatter;
use PhpSpec\ObjectBehavior;

class ApiResponseFormatterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ApiResponseFormatter::class);
    }

    public function let(\GuzzleHttp\Psr7\Response $response)
    {
        $this->beConstructedThrough('format', [$response]);
    }

    public function it_should_be_able_to_format_response_as_array(
        \GuzzleHttp\Psr7\Response $response,
        \GuzzleHttp\Psr7\Stream $stream
    ) {
        $data = [
            'self' => 'https://example.com',
            'emailAddress' => 'test@example.com',
        ];
        $response->getBody()
            ->willReturn($stream)
            ->shouldBeCalled();
        $stream->getContents()
            ->willReturn(json_encode($data))
            ->shouldBeCalled();
        $this->format($response)->asArray()->shouldBeArray();
    }

    public function it_should_be_able_to_format_response_as_object(
        \GuzzleHttp\Psr7\Response $response,
        \GuzzleHttp\Psr7\Stream $stream
    ) {
        $data = [
            'self' => 'https://example.com',
            'emailAddress' => 'test@example.com',
        ];
        $response->getBody()
            ->willReturn($stream)
            ->shouldBeCalled();
        $stream->getContents()
            ->willReturn(json_encode($data))
            ->shouldBeCalled();
        $this->format($response)->asObject()->shouldBeAnInstanceOf(\stdClass::class);
    }
}
