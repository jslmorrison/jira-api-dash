<?php

namespace spec\App\Service;

use App\Service\MyDetails;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MyDetailsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MyDetails::class);
    }

    public function let(\App\Service\HttpClient $apiClient)
    {
        $this->beConstructedWith($apiClient);
    }

    public function it_should_be_able_fetch_response_from_api(
        \App\Service\HttpClient $apiClient,
        \GuzzleHttp\Client $client,
        \GuzzleHttp\Psr7\Response $response
    ) {
        $client->request(
            'GET', 'myself', ['auth' => ['username-string', 'token-string']]
        )->shouldBeCalled()->willReturn($response);
        $apiClient->client()->willReturn($client)->shouldBeCalled();
        $apiClient->username()->willReturn('username-string')->shouldBeCalled();
        $apiClient->token()->willReturn('token-string')->shouldBeCalled();
        $this->fetch();
    }
}
