# Jira Api Dash

Make example calls to the Jira api.

## Installation
```
docker-compose up [-d]
```

## Running tests
```
./vendor/bin/phpspec run
```

## Usage
* Create env.local file
* Configure JIRA_API_* values in .env to .env.local file with your own values.

Web: http://localhost/my-details

CLI: bin/console app:my-details