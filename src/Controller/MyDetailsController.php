<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Service\MyDetails;
use App\Service\ApiResponseFormatter;

class MyDetailsController extends AbstractController
{
    public function __invoke(MyDetails $myDetailsService)
    {
        $response = $myDetailsService->fetch();
        $myDetails = ApiResponseFormatter::format($response)->asArray();
       
        return $this->render(
            'my-details/index.html.twig',
            [
                'myDetails' => $myDetails
            ]
        );
    }
}