<?php

namespace App\Service;

class ApiResponseFormatter
{
    private $response;

    private function __construct()
    {
    }

    public static function format(\GuzzleHttp\Psr7\Response $response): self
    {
        $apiResponseFormatter = new ApiResponseFormatter();
        $apiResponseFormatter->response = $response;

        return $apiResponseFormatter;
    }

    public function asArray(): array
    {
        return json_decode(
            $this->response->getBody()->getContents(), true
        );
    }

    public function asObject(): \stdClass
    {
        return json_decode(
            $this->response->getBody()->getContents()
        );
    }
}
