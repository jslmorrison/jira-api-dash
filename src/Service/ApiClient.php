<?php

namespace App\Service;

class ApiClient implements HttpClient
{
    private $username;
    private $token;
    private $baseUri;

    public function __construct(
        string $username,
        string $token,
        string $baseUri
    ) {
        $this->username = $username;
        $this->token = $token;
        $this->baseUri = $baseUri;
    }

    public function client(): \GuzzleHttp\Client
    {
        return new \GuzzleHttp\Client(
            [
                'base_uri' => $this->baseUri,
            ]
        );
    }

    public function username(): string
    {
        return $this->username;
    }

    public function token(): string
    {
        return $this->token;
    }
}
