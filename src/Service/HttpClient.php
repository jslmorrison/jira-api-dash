<?php

namespace App\Service;

interface HttpClient
{
    public function client(): \GuzzleHttp\Client;
    public function username(): string;
    public function token(): string;
}
