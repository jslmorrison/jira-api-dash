<?php

namespace App\Service;

class MyDetails
{
    private $apiClient;

    public function __construct(\App\Service\HttpClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function fetch(): \GuzzleHttp\Psr7\Response
    {
        $response = $this->apiClient
            ->client()
            ->request(
                'GET',
                'myself',
                [
                    'auth' => [
                        $this->apiClient->username(),
                        $this->apiClient->token()
                    ]
                ]
            );
        return $response;
    }
}
