<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\MyDetails;
use App\Service\ApiResponseFormatter;

/**
 * Class My DetailsCommand
 * 
 * @package App\Command
 */
class MyDetailsCommand extends Command
{
    private $myDetailsService;
    protected static $defaultName = 'app:my-details';

    public function __construct(MyDetails $myDetailsService)
    {
        $this->myDetailsService = $myDetailsService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Get my details from api');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $io = new SymfonyStyle($input, $output);
            $io->title('Fetching my details');
            $response = $this->myDetailsService
                ->fetch();
            // dump(ApiResponseFormatter::format($response)->asArray());
            dump(ApiResponseFormatter::format($response)->asObject());
        } catch (\Throwable $e) {
            $io->error('Oops: '.$e->getMessage()); // todo 
        } finally {
            return 0;
        }
    }
}
